package org.user.services;

import java.util.Base64;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.user.model.User;
import org.user.repository.UserRepository;

@Service
public class UserService {

	private final UserRepository userRepository;

	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	public User getUser(Long userID) throws Exception{
		return userRepository.findById(userID).orElseThrow(() -> new Exception("Data for user id not present"));
	}
	
	public Optional<User> addUser(User user){
		user.setPassword(Base64.getEncoder().encodeToString(user.getPassword().getBytes()));
		return Optional.ofNullable(userRepository.save(user));
	}
	
	public Optional<User> updateUser(User oldUser,User newUser){
		return Optional.ofNullable(userRepository.save(newUser));
	}
	
	public User deleteUser(Long userID){
		return new User();
	}
	
}
