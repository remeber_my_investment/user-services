package org.user.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="USERS")
public class User implements Serializable{
	private static final long serialVersionUID = 7687722589781497535L;
	@Id
	@GeneratedValue
	@Column(name="USER_ID")
	private Long userId;
	
	@NotNull(message="First Name can't be Null")
	@Column(name="FIRST_NAME")
	private String firstName;
	
	@NotNull(message="Last Name can't be Null")
	@Column(name="LAST_NAME")
	private String lastName;
	
	@NotNull(message="Email can't be Null")
	@Column(name="EMAIL_ID")
	private String email;
	
	@NotNull(message="Username can't be Null")
	@Column(name="USERNAME")
	private String userName;
	
	@NotNull(message="Password can't be Null")
	@Column(name="PASSWORD")
	private String password;
	
	@NotNull(message="Contact Number can't be Null")
	@Column(name="CONATCT_NUMBER")
	private String contactNumber;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	
	//Add CREATE/UPDATE DATE and USER column for Audit Purpose
	
	
}
