package org.user.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.user.model.User;
import org.user.services.UserService;

@RestController
@RequestMapping("/api/users")
public class UserController {
	private final UserService userService;

	public UserController(UserService userService) {
		super();
		this.userService = userService;
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<User> getUser(@PathVariable("id") String id,HttpServletResponse response) throws Exception{
		setCookie(response);
		return ResponseEntity.ok(userService.getUser(Long.valueOf(id)));
	}
	
	@PostMapping
	public ResponseEntity<?>  addUser(@Valid @RequestBody User user,BindingResult result){
		if(result.hasErrors()) {
			List<String> errorList = result.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList());
			return ResponseEntity.badRequest().body(errorList);
		}else {
			return this.userService.addUser(user).map(inv->{
				URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(user.getUserId()).toUri();
				return ResponseEntity.created(location).build();
			}).orElse(ResponseEntity.noContent().build());
		}
	}
	
	@PutMapping
	public  ResponseEntity<?> updateUser(@Valid @RequestBody User user,BindingResult result) throws Exception{
		if(result.hasErrors()) {
			List<String> errorList = result.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList());
			return ResponseEntity.badRequest().body(errorList);
		}else {
			User oldUser = userService.getUser(user.getUserId());
			return userService.updateUser(oldUser, user).map(t -> {
				return ResponseEntity.ok(t);
			}).orElse(ResponseEntity.noContent().build());
		}
	}
	
	@DeleteMapping("/{id}")
	public void deleteUser(@PathParam("id")Long investId) throws Exception{
		userService.deleteUser(investId);
	}
	
	public void setCookie(HttpServletResponse response) {
		Cookie cookie = new Cookie("username","soodank");
		cookie.setDomain(".herokuapp.com");
		cookie.setMaxAge(60*5); //valid for 5 minutes
		cookie.setComment("Cookie has the value of Session ID");
		cookie.setPath("/"); //Global cookie accessible everywhere
		response.addCookie(cookie);
	}
	
	public void deleteCookie(HttpServletResponse response) {
		Cookie cookie = new Cookie("username",null);
		cookie.setDomain(".herokuapp.com");
		cookie.setMaxAge(0); //valid for 5 minutes
		cookie.setComment("Cookie has the value of Session ID");
		cookie.setPath("/"); //Global cookie accessible everywhere
		response.addCookie(cookie);
	}
}
