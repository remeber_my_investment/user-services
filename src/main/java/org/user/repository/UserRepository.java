package org.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.user.model.User;

public interface UserRepository extends JpaRepository<User,Long>{

}
